import { createRouter, createWebHashHistory } from 'vue-router'
import Inicio from '../views/Inicio.vue'
import Persona from '../views/Persona.vue'
import Usuarios from '../views/Usuarios.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Inicio
  },
  {
    path: '/usuariosApi',
    name: 'Usuarios',
    component: Usuarios
  },

  {
    path: '/persona',
    name: 'Persona',
    component: Persona
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
